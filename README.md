# jasp-decrash

## Why?

Jedi Academy crashes after a certain number of map loads. That includes map
changes and also quickloading. It happens because on each load, certain texture
data is copied into memory again and again, exhausting the space accessible to
the game, until eventually loading *anything* just fails. The problem is more or
less severe depending on your hardware, video drivers, and the phase of the
moon.

The awesome [Speed-Academy](https://github.com/kugelrund/Speed-Academy) build by
Sphere fixes these issues, but that build is not yet approved for JKA speedruns
on speedrun.com (and probably elsewhere), so you have to work around the crashes
by periodically restarting the game's video engine (using the "vid_restart"
console command which you are allowed to bind to a key).

Still, that's rather tedious. Enter jasp-decrash.

## How?

While jasp-decrash is running, it periodically searches for a running JKA game.
When it finds one, it patches the game code directly in your RAM, so that the
game files do not need to be modified. It inserts texture unloading code in the
places where the developers decided to not put it. The code only differs during
map loading.

### But HOW???

I spent hours staring at machine code. Don't ask.

## Pitfalls

When using the Steam version of the game, occasionally jasp-decrash will detect
it while the Steam DRM is still spinning up, and therefore fail to verify the
code. It will retry a few times but sometimes Steam DRM is particularly slow
and eventually jasp-decrash will give up.  When this happens, just restart the
game; the second time around, Steam DRM is usually fast enough.
