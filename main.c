#define _WIN32_WINNT 0x0600
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <shellapi.h>
#include <tlhelp32.h>

#define VERSION "0.5"
#define PATCH_MAX_RETRIES 5

#define HOOK1 ((void *)0x439c5b)
#define HOOK2 ((void *)0x46a413)
#define HOOK3 ((void *)0x46a6a4)
#define HOOK4 ((void *)0x46a6d8)
#define HOLE ((void *)0x4efc80)

enum patch_result {
	PATCH_DONE,

	PATCH_RETRY_MIN,
	PATCH_RETRY_UNRECOGNIZED1,
	PATCH_RETRY_MAX,

	PATCH_SKIP_MIN,
	PATCH_SKIP_APPLIED1,
	PATCH_SKIP_MAX,

	PATCH_FAIL_MIN,
	PATCH_FAIL_READ1,
	PATCH_FAIL_READ2,
	PATCH_FAIL_PARTIAL,
	PATCH_FAIL_UNRECOGNIZED2,
	PATCH_FAIL_SUSPEND,
	PATCH_FAIL_ADDCODE,
	PATCH_FAIL_1,
	PATCH_FAIL_2,
	PATCH_FAIL_3,
	PATCH_FAIL_4,
	PATCH_FAIL_MAXRETRIES,
	PATCH_FAIL_MAX,
};
#define is_patch_type(x, type) (x > PATCH_ ## type ## _MIN && x < PATCH_ ## type ## _MAX)
#define is_patch_retry(x) is_patch_type(x, RETRY)
#define is_patch_skip(x) is_patch_type(x, SKIP)
#define is_patch_fail(x) is_patch_type(x, FAIL)
#define is_patch_done(x) (x == PATCH_DONE)

DWORD prev_pid = 0;
DWORD prev_retry = 0;

static const char *clsname = "jasp-decrash-msgwin";
LRESULT CALLBACK WinProc(HWND, UINT, WPARAM, LPARAM);
HINSTANCE hInstance;
WNDCLASSEX wcls = {};
HWND mainWindow;
NOTIFYICONDATA nid = {};
HMENU menu;

/* Output helpers {{{ */
void putnotify(char *msg, char *title)
{
	nid.uFlags |= NIF_INFO | NIF_REALTIME;
	nid.dwInfoFlags = NIIF_LARGE_ICON;
	strncpy(nid.szInfo, msg, ARRAYSIZE(nid.szInfo));
	strncpy(nid.szInfoTitle, title, ARRAYSIZE(nid.szInfo));
	Shell_NotifyIcon(NIM_MODIFY, &nid);
}

void puterror(char *msg)
{
	putnotify(msg, "jasp-decrash failed");
}

void putwarning(char *msg)
{
	putnotify(msg, "jasp-decrash warning");
}

void putinfo(char *msg)
{
	putnotify(msg, "jasp-decrash");
}

void putresult(int status)
{
	char *msg = NULL;
	char buf[100];
	switch (status) {
		case PATCH_DONE:
			msg = "Crash reduction successfully activated."; break;
		case PATCH_SKIP_APPLIED1:
			msg = "Patch already applied - at least partially. Skipping."; break;
		case PATCH_FAIL_READ1:
			msg = "Cannot access game process!"; break;
		case PATCH_RETRY_UNRECOGNIZED1:
			msg = "Unrecognized game version - will retry in a bit"; break;
		case PATCH_FAIL_READ2:
			msg = "Cannot access game process (only during second try, weird)!"; break;
		case PATCH_FAIL_PARTIAL:
			msg = "Patch was already applied partially, strange..."; break;
		case PATCH_FAIL_UNRECOGNIZED2:
			msg = "Unrecognized version during second check."; break;
		case PATCH_FAIL_SUSPEND:
			msg = "Failed suspending process, patching is too risky."; break;
		case PATCH_FAIL_ADDCODE:
			msg = "Failed adding our crash reduction code!"; break;
		case PATCH_FAIL_1:
			msg = "First patch for crash reduction failed!"; break;
		case PATCH_FAIL_2:
			msg = "Second patch for crash reduction failed!"; break;
		case PATCH_FAIL_3:
			msg = "Third patch for crash reduction failed!"; break;
		case PATCH_FAIL_4:
			msg = "Fourth patch for crash reduction failed!"; break;
		case PATCH_FAIL_MAXRETRIES:
			msg = "Tried patching a bunch of times but still doesn't work, giving up now."; break;
		default:
			snprintf(buf, 100, "Something unexpected happened while trying to patch, code %d - please report!", status);
			msg = buf;
	}
	if (is_patch_done(status)) putinfo(msg);
	else if (is_patch_skip(status)) putwarning(msg);
	else if (is_patch_retry(status)) /* nothing */;
	else {
		char buf[256];
		snprintf(buf, 256, "%s\nPatch was not applied; please restart game to retry.", msg);
		puterror(buf);
	}
}

void putwinerror(char *msg)
{
	char buf[500];
	strncpy(buf, msg, 500);
	strncat(buf, ": ", 500);
	int len = strlen(buf);
	char *ptr = buf + len;
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, ptr, 500 - len, NULL);
	puterror(buf);
}

/* Output helpers }}} */

int process(DWORD pid)
{
	int status = PATCH_DONE;
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	unsigned char testbuf[5];
	unsigned char testexpected[] = "\xe8\x70\x0e\xfe\xff"; /* CALL 0x47f260 = shader clear */
	unsigned char patched[] = "\xe8\x50\x60\x0b\x00"; /* CALL 0x4efcb0, our first patch */
	unsigned char testexpected2[] = "\xe8\x48\x4e\x01\x00"; /* CALL 0x41aad0 = Hunk_Clear */
	unsigned char patched2[] = "\xe8\xa8\x58\x08\x00"; /* CALL 0x4efcc0, our second patch */
	/*
	 * RE_Shutdown: in dynamic glow branch, use our routine for deleting the helper textures
	 * so the pointers get cleared
	 */
	unsigned char patched3[] = "\xe8\xd7\x55\x08\x00" /* CALL 0x4efc80, our main delete routine */
		"\xeb\x20" /* JMP EIP+0x20 = skip rest of original code */
		;
	unsigned char patched4[] = "\xe8\xf3\x55\x08\x00"; /* CALL 0x4efcd0, patch 4 below */
	unsigned char code[] =
		"\x56\x57\x53" /* PUSH ESI/EDI/EBX */
		"\xbe\x9c\x85\xc9\0" /* MOV ESI, 0xc9859c = texture base address */
		"\x8b\x3d\xf4\xb7\xd1\0" /* MOV EDI, DWORD PTR [0xd1b7f4] = glDeleteTextures proc addr */
		"\x31\xdb\x31\xc9\xb1\x03" /* XOR EBX, EBX; XOR ECX, ECX; MOV CL, 3 = loop counter */

		/* loop begin */
		"\x83\x3e\x00" /* CMP DWORD PTR [ESI], 0 = texture unset? */
		"\x74\x09" /* JE EIP+9 = skip delete */
		"\x51\x56\x6a\x01" /* PUSH ECX; PUSH ESI; PUSH 1 = one texture */
		"\xff\xd7" /* CALL EDI = glDeleteTextures */
		"\x59\x89\x1e" /* POP ECX; MOV DWORD PTR [ESI], EBX = clear texture entry */
		"\x83\xc6\x04\x49\x75\xec" /* ADD ESI, 4; DEC ECX; JE EIP-0x14 = repeat loop with next entry */

		"\x5b\x5f\x5e\xc3" /* POP EBX; POP EDI; POP ESI; RET */
		"\x90\x90\x90\x90" /* padding */

		/* patch 1 */
		"\xe8\xcb\xff\xff\xff" /* CALL 0x4efc80 = see above */
		"\xe9\x16\xae\xf2\xff" /* JMP 0x41aad0 = Hunk_Clear, the call we replaced */
		"\x90\x90\x90\x90\x90\x90" /* padding */

		/* patch 2 */
		"\xe8\xbb\xff\xff\xff" /* CALL 0x4efc80 = see above */
		"\xe9\x96\xf5\xf8\xff" /* JMP 0x47f260 = ShaderEntryPtrs_Clear, the call we replaced */
		"\x90\x90\x90\x90\x90\x90" /* padding */

		/*
		 * patch 4 (patch 3 needs no separate code)
		 * RE_Shutdown: overwrite GLimp_Shutdown with a routine of ours that clears the pointers for the
		 * glow helper textures after shutdown (so we don't attempt to clear them again after GL has been
		 * re-inited)
		 * We don't have to save EDI here because the caller restores it immediately afterwards
		 */
		"\xe8\x8b\xcb\xf9\xff" /* CALL GLimp_Shutdown */
		"\xbf\x9c\x85\xc9\0" /* MOV EDI, 0xc9859c = texture base address */
		"\x31\xc0" /* XOR EAX, EAX */
		"\xfc\xab\xab\xab" /* CLD; 3x STOSD */
		"\xc3" /* RET */
		;

#	define result(code) { status = code; goto closeproc; }

	BOOL res = ReadProcessMemory(hProcess, HOOK1, &testbuf, 5, NULL);
	if (!res) result(PATCH_FAIL_READ1)
	if (!memcmp(&testbuf, &patched, sizeof (testbuf))) result(PATCH_SKIP_APPLIED1)
	if (memcmp(&testbuf, &testexpected, sizeof (testbuf))) result(PATCH_RETRY_UNRECOGNIZED1)
	res = ReadProcessMemory(hProcess, HOOK2, &testbuf, sizeof (testbuf), NULL);
	if (!res) result(PATCH_FAIL_READ2)
	if (!memcmp(&testbuf, &patched2, sizeof (testbuf))) result(PATCH_FAIL_PARTIAL)
	if (memcmp(&testbuf, &testexpected2, sizeof (testbuf))) result(PATCH_FAIL_UNRECOGNIZED2)
	/* Living on the edge, don't check original code for remaining hooks */

	/* Suspend process so we don't get interrupted, cheaty way */
	res = DebugActiveProcess(pid);
	if (!res) result(PATCH_FAIL_SUSPEND)
	DebugSetProcessKillOnExit(FALSE);

#	undef result
#	define result(code) { status = code; goto undebug; }

	/* Overwrite, finally */
	res = WriteProcessMemory(hProcess, HOLE, &code, sizeof (code)-1, NULL);
	if (!res) result(PATCH_FAIL_ADDCODE)
	res = WriteProcessMemory(hProcess, HOOK1, &patched, sizeof (patched)-1, NULL);
	if (!res) result(PATCH_FAIL_1)
	res = WriteProcessMemory(hProcess, HOOK2, &patched2, sizeof (patched2)-1, NULL);
	if (!res) result(PATCH_FAIL_2)
	res = WriteProcessMemory(hProcess, HOOK3, &patched3, sizeof (patched3)-1, NULL);
	if (!res) result(PATCH_FAIL_3)
	res = WriteProcessMemory(hProcess, HOOK4, &patched4, sizeof (patched4)-1, NULL);
	if (!res) result(PATCH_FAIL_4)
undebug:
	DebugActiveProcessStop(pid);
closeproc:
	CloseHandle(hProcess);
	return status;
}

#define MENU_ABOUT 0
#define MENU_EXIT 1
#define WM_NOTIFYICON WM_USER

void Scan()
{
	int found = 0;
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof (PROCESSENTRY32);
	
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snapshot == INVALID_HANDLE_VALUE) {
		putwinerror("Can't get list of running processes");
		return;
	}
	if (Process32First(snapshot, &entry) == TRUE) {
		do {
			if (strcasecmp(entry.szExeFile, "jasp.exe")) continue;
			found++;
			if (found > 1)
				putwarning("Multiple matching processes found, maybe you have a crashed game in the background?");
			if (entry.th32ProcessID == prev_pid) {
				if (prev_retry == PATCH_MAX_RETRIES) continue;
			} else {
				prev_pid = entry.th32ProcessID;
				prev_retry = 0;
			}
			int status = process(entry.th32ProcessID);
			if (is_patch_retry(status)) {
				prev_retry++;
				if (prev_retry == PATCH_MAX_RETRIES) status = PATCH_FAIL_MAXRETRIES;
			} else
				prev_retry = PATCH_MAX_RETRIES; /* don't try again */
			putresult(status);
			if (is_patch_skip(status)) continue; else break;
		} while (Process32Next(snapshot, &entry) == TRUE);
	} else {
		putwinerror("Can't process list of running processes");
	}
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;
	BOOL bRet;

	hInstance = hInst;
	wcls.cbSize = sizeof (wcls);
	wcls.lpfnWndProc = WinProc;
	wcls.hInstance = hInstance;
	wcls.lpszClassName = clsname;
	RegisterClassEx(&wcls);
	mainWindow = CreateWindowEx(0, clsname, NULL, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, hInstance, NULL);

	menu = CreatePopupMenu();
	AppendMenu(menu, MF_STRING, MENU_ABOUT, "About...");
	AppendMenu(menu, MF_STRING, MENU_EXIT, "Exit");

	msg.wParam = 0;
	while (TRUE) {
		bRet = GetMessage(&msg, NULL, 0, 0);
		if (bRet <= 0) break;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	Shell_NotifyIcon(NIM_DELETE, &nid);
	return msg.wParam;
}

LRESULT CALLBACK WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_CREATE:
			nid.cbSize = sizeof (nid);
			nid.hWnd = hWnd;
			nid.uCallbackMessage = WM_NOTIFYICON;
			nid.uFlags = NIF_ICON | NIF_TIP | NIF_SHOWTIP | NIF_MESSAGE;
			nid.uID = 0;
			strncpy(nid.szTip, "jasp-decrash", ARRAYSIZE(nid.szTip));
			nid.hIcon = LoadIcon(hInstance, "ico16");
			nid.uVersion = NOTIFYICON_VERSION_4;
			Shell_NotifyIcon(NIM_ADD, &nid);
			Shell_NotifyIcon(NIM_SETVERSION, &nid);

			/* Trigger first scan */
			PostMessage(hWnd, WM_TIMER, 1, 0);
			break;
		case WM_NOTIFYICON:
			switch (LOWORD(lParam)) {
				case WM_CONTEXTMENU:
					TrackPopupMenu(menu, 0, GET_X_LPARAM(wParam), GET_Y_LPARAM(wParam), 0, mainWindow, NULL);
					break;
				default:
					/* nothing */
					;
			}
			break;
		case WM_COMMAND:
			switch (LOWORD(wParam)) {
				case MENU_ABOUT:
					MessageBox(hWnd,
						"jasp-decrash " VERSION ", written by gravydanger, https://gitlab.com/gravydanger\n"
						"Based on code troubleshooting by Sphere, https://github.com/kugelrund",
						"About jasp-decrash",
						MB_ICONINFORMATION);
					break;
				case MENU_EXIT:
					PostQuitMessage(0);
					break;
			}
			break;
		case WM_TIMER:
			Scan();
			SetTimer(hWnd, 1, 5000, NULL);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
