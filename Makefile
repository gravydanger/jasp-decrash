# Makefile designed for Cygwin with mingw64-i686
# PATH="/usr/i686-w64-mingw32/bin:$(PATH)" make

TARGET=i686-w64-mingw32

all: jasp-decrash.exe

clean:
	rm -f *.o *.exe

jasp-decrash.exe: main.o resources.o
	$(TARGET)-gcc -o jasp-decrash.exe -Wl,--subsystem,windows -m32 main.o resources.o -lcomctl32 

resources.o: resources.rc icon.ico icon16.ico
	$(TARGET)-windres resources.rc resources.o

main.o: main.c
	$(TARGET)-gcc -m32 -c main.c
